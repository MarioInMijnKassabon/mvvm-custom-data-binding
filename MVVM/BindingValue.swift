//
//  BindingValue.swift
//  MVVM
//
//  Created by Sjahriyar Soheilizadeh on 19/11/2019.
//  Copyright © 2019 Sjahriyar Soheytizadeh. All rights reserved.
//

import Foundation

typealias CompletionHandler = (() -> Void)

/**
 This class is the property observer
 */
class BindingValue<T>
{
    
    var value : T {
        didSet {
            self.notify()
        }
    }
    
    private var observers = [String: CompletionHandler]()

    init(_ value: T) {
        self.value = value
    }
    
    public func addObserver(_ observer: NSObject, completionHandler: @escaping CompletionHandler) {
        observers[observer.description] = completionHandler
    }

    public func addAndNotify(observer: NSObject, completionHandler: @escaping CompletionHandler) {
        self.addObserver(observer, completionHandler: completionHandler)
        self.notify()
    }

    private func notify() {
        observers.forEach({ $0.value() })
    }

    deinit {
        observers.removeAll()
        print("🔆 \(String(describing: self)) Deinitialized")
    }
}

