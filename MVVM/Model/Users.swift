//
//  QuoteModel.swift
//  MVVM
//
//  Created by Sjahriyar Soheilizadeh on 19/11/2019.
//  Copyright © 2019 Sjahriyar Soheytizadeh. All rights reserved.
//

import Foundation

struct Users: Decodable {
    let first: String
    let email: String
}
