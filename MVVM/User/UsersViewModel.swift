//
//  UsersViewModel.swift
//  MVVM
//
//  Created by Sjahriyar Soheilizadeh on 19/11/2019.
//  Copyright © 2019 Sjahriyar Soheytizadeh. All rights reserved.
//

import Foundation

struct UsersViewModel
{

    weak var dataSource : DataSource<Users>?
    
    init(dataSource : DataSource<Users>?) {
        self.dataSource = dataSource
    }
    
    func performAPICall(_ completion: @escaping (Result<[Users], Error>) -> ()) {
        var request = URLRequest(url: URL(string: "https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole")!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (d, r, e) in
            
            guard e == nil else { completion(.failure(e!))
                return
            }
            
            if let data = d {
                
                let jsonDecoder = JSONDecoder()
                if let dictionaryFromJSON = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                    let jsonData = try? JSONSerialization.data(withJSONObject: dictionaryFromJSON, options: [])
                    
                    if let decodedData = try? jsonDecoder.decode([Users].self, from: jsonData!) {
                        completion(.success(decodedData))
                    }
                }

            }
        }
        
        task.resume()
    }
    

    // API Call to fetch data and update the dataSource
    func fetchCourses() {
        
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) { (_) in
            self.performAPICall { (data) in
                // Set the generic data source value
                DispatchQueue.main.async {
                    switch data {
                    case .success(let users):
                        self.dataSource?.data.value = users
                    case .failure(let error):
                        print("🤮 \(error)")
                    }
                    
                }
            }
        }

    }
    
}
