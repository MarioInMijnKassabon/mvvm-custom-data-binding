//
//  TableViewDataSource.swift
//  MVVM
//
//  Created by Sjahriyar Soheilizadeh on 19/11/2019.
//  Copyright © 2019 Sjahriyar Soheytizadeh. All rights reserved.
//

import UIKit

class TableViewDataSource : DataSource<Users>, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CourseTableViewCell
        
        let user = data.value[indexPath.row]
        
        cell.textLabel?.text = user.first
        cell.detailTextLabel?.text = "E-mail: \(String(describing: user.email))"
        
        return cell
    }
}
