//
//  UsersController.swift
//  MVVM
//
//  Created by Sjahriyar Soheilizadeh on 19/11/2019.
//  Copyright © 2019 Sjahriyar Soheytizadeh. All rights reserved.
//

import UIKit

class UsersController: UITableViewController
{

    var dataSource = TableViewDataSource()
    
    // MARK: View Model
    
    lazy var viewModel: UsersViewModel = {
        let viewModel = UsersViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
    
    func setupTableView() {
        
        tableView.register(CourseTableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView.dataSource = dataSource
        
        // View Model will notify the view on data change.
        self.dataSource.data.addAndNotify(observer: self) { [weak self] in
            self?.tableView.reloadData()
        }
        
        self.viewModel.fetchCourses()
    }
}

