//
//  DataSource.swift
//  MVVM
//
//  Created by Sjahriyar Soheilizadeh on 19/11/2019.
//  Copyright © 2019 Sjahriyar Soheytizadeh. All rights reserved.
//

import Foundation

/**
 This class is meant to separate the ViewModel to the data layer.
 */
class DataSource<T>: NSObject
{
    // Initialize with empty array
    var data: BindingValue<[T]> = BindingValue([])
}
