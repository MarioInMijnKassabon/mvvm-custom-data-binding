//
//  MVVMTests.swift
//  MVVMTests
//
//  Created by Sjahriyar Soheilizadeh on 19/11/2019.
//  Copyright © 2019 Sjahriyar Soheytizadeh. All rights reserved.
//

import XCTest
@testable import MVVM

class MVVMTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewModel() {
        let users = [
            Users(first: "George", email: "george@gmail.com"),
            Users(first: "Daniel", email: "daniel@gmail.com")
        ]
        
        let dataSource = DataSource<Users>()
        
        let usersViewModel = UsersViewModel(dataSource: dataSource)
        usersViewModel.dataSource?.data.value = users
        
        XCTAssertEqual(dataSource.data.value.first!.first, users.first!.first)
        XCTAssertEqual(dataSource.data.value.first!.email, users.first!.email)
    }

}
